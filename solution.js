const fetch = require('node-fetch');

const usersUrl = 'https://jsonplaceholder.typicode.com/users';
const todoUrl = 'https://jsonplaceholder.typicode.com/todos';

//1. Fetch all the users
const fetchAllUsers = () => {
    fetch(usersUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.json();
        })
        .then((response) => {
            console.log(JSON.stringify(response, null, 4));
        })
        .catch((error) => {
            console.log(error);
        });
}

//2. Fetch all the todos
const fetchAllTodos = () => {
    fetch(todoUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.json();
        })
        .then((response) => {
            console.log(JSON.stringify(response, null, 4));
        })
        .catch((error) => {
            console.log(error);
        });
}

//3. Use the promise chain and fetch the users first and then the todos.
const fetchUsersThenTodos = () => {
    fetch(usersUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.json();
        })
        .then((userData) => {
            console.log(JSON.stringify(userData, null, 4));
            return fetch(todoUrl);
        })
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.json();
        })
        .then((todoData) => {
            console.log(JSON.stringify(todoData, null, 4));
        })
        .catch((error) => {
            console.log(error);
        });
}

// 4. Use the promise chain and fetch the users first and then all the details for each user.
const fetchUsersThenEveryUserInfo = () => {

    //this function fetch user detail by Id
    const getUserDetailById = (id) => {
        return new Promise((resolve, reject) => {
            if (id === undefined) {
                const error = 'Id must be passed';
                reject(error);
            }
            if (typeof id !== 'number') {
                const error = 'Id must be of type number';
                reject(error);
            }

            const eachUserUrl = `https://jsonplaceholder.typicode.com/users?id=${id}`;
            fetch(eachUserUrl)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error('Network response was not OK');
                    }
                    return response.json();
                })
                .then((userData) => {
                    resolve(userData);
                })
                .catch((error) => {
                    reject(error);
                })
        });
    }

    fetch(usersUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.json();
        })
        .then((users) => {
            const userIds = users.map((user) => {
                return user.id;
            });
            const usersInfoByIds = userIds.map((userId) => {
                return getUserDetailById(userId);
            });
            return Promise.all(usersInfoByIds);
        })
        .then((userDetails) => {
            console.log(JSON.stringify(userDetails, null, 4));
        })
        .catch((error) => {
            console.log(error);
        });
}
